﻿using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using System;

namespace OfficeTools.Shared
{
    public class EmployeeManager
    {
        public EmployeeListResponse GetEmployeeList()
        {
            EmployeeListResponse employeeListResponse = new EmployeeListResponse();

            using (var client = new RestClient(new Uri("http://192.168.1.118:3000/")))
            {
                ServiceRequest serviceRequest = new ServiceRequest("hrms", "employeelist");
                var request = new RestRequest("api/invoke", Method.POST);
                request.AddJsonBody(serviceRequest);
                var result = client.Execute<EmployeeListResponse>(request);
            }
            return employeeListResponse;
        }
    }
}
