using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Classes;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Adapters
{
    class NavigationListAdapter : BaseAdapter
    {
        private readonly Context context;
        private readonly List<NavigationDrawerItem> navDrawerItems;
        public NavigationListAdapter(Context context, List<NavigationDrawerItem> navDrawerItems)
        {
            this.context = context;
            this.navDrawerItems = navDrawerItems;
        }

        public override int Count
        {
            get
            {
                return navDrawerItems.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;

        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = this.navDrawerItems[position];
            if (convertView == null)
            {
                LayoutInflater mInflater = (LayoutInflater)context.GetSystemService(Activity.LayoutInflaterService);
                convertView = mInflater.Inflate(Resource.Layout.drawer_list_item, parent, false);
            }
            ImageView imgIcon = (ImageView)convertView.FindViewById(Resource.Id.icon);
            imgIcon.SetImageResource(item.Icon);
            TextView txtTitle = (TextView)convertView.FindViewById(Resource.Id.title);
            txtTitle.SetText(item.Title, TextView.BufferType.Normal);
            return convertView;
        }
    }
}