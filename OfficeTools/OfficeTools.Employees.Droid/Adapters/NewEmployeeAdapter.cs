using Android.Support.V4.App;
using Android.Views;
using OfficeTools.Employees.Droid.Fragments;
using OfficeTools.Shared.Models;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Adapters
{
    public class NewEmployeeAdapter : FragmentPagerAdapter
    {

        private readonly List<Fragment> CardsFragments = new List<Fragment>();

        public NewEmployeeAdapter(FragmentManager fm, List<NewEmployee> newEmployees, ContextThemeWrapper context) : base(fm)
        {
            foreach (var newEmployee in newEmployees)
                CardsFragments.Add(new NewEmployeeFragment(newEmployee));
        }

        public override int Count
        {
            get
            {
                return CardsFragments.Count;
            }
        }

        public override Fragment GetItem(int position)
        {
            return CardsFragments[position];
        }
    }
}
