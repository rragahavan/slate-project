using Android.Support.V4.App;
using Android.Views;
using OfficeTools.Employees.Droid.Fragments;
using OfficeTools.Shared.Models;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Adapters
{
    public class BirthdayViewPagerAdapter : FragmentPagerAdapter
    {

        private readonly List<Fragment> CardsFragments;
        public BirthdayViewPagerAdapter(FragmentManager fm, List<UpcomingBirthday> upcomingBirthdays, ContextThemeWrapper context) : base(fm)
        {
            CardsFragments = new List<Fragment>();
            foreach (var upComingBirthdays in upcomingBirthdays)
                CardsFragments.Add(new ViewPagerFragment(upComingBirthdays));
        }


        public override int Count
        {
            get
            {
                return CardsFragments.Count;
            }
        }


        public override Fragment GetItem(int position)
        {
            return CardsFragments[position];
        }
    }
}