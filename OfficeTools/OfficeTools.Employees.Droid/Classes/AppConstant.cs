using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OfficeTools.Employees.Droid.Classes
{
    public static class AppConstant
    {

        public const string CLIENTID =  "4d3fb37a-cd62-489a-89ec-20693539abae";
        public const string AUTHORITY = "https://login.microsoftonline.com/28f338ff-2bcb-42b1-887d-5742265cbfba/oauth2/authorize";
        public const string RETURNURI = "http://goavegaoffice.cloudapp.net/auth/callback";
        public const string GRAPHRESOURCEURI = "https://graph.windows.net";
    }
}