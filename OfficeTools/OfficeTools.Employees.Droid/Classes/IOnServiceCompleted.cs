namespace OfficeTools.Employees.Droid.Classes
{
    public interface IOnServiceCompleted
    {
        void OnTaskCompleted(Java.Lang.Object result);

    }
}