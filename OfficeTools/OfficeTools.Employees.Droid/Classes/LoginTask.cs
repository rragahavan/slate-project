
using Android.App;
using Android.Content;
using Android.OS;
using OfficeTools.Employees.Droid.Repositories;

namespace OfficeTools.Employees.Droid.Classes
{
    public class LoginTask : AsyncTask
    {
        private ProgressDialog progressDialog;
        private readonly Context context;
        private readonly string token;
        private readonly IOnTaskCompleted taskCompleted;

        public LoginTask(Context context, string token, IOnTaskCompleted taskCompleted)
        {
            this.context = context;
            this.token = token;
            this.taskCompleted = taskCompleted;
        }
        protected override void OnPreExecute()
        {
            base.OnPreExecute();
            progressDialog = ProgressDialog.Show(context, "Login Progress", "Please Wait......");

        }
        protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
        {
            var loginResponse = new EmployeeRepository(context).GetAccessToken(token);

            if (loginResponse.Success)
            {
                AppPreferences.SaveAccessToken(loginResponse.Data.Token);
                AppPreferences.SaveFirstName(loginResponse.Data.User.FirstName);
                AppPreferences.SaveLastName(loginResponse.Data.User.LastName);
                AppPreferences.SaveImagePath(loginResponse.Data.User.ProfileImage);
            }
            else
                return false;
            return loginResponse.Success;
        }
        protected override void OnPostExecute(Java.Lang.Object result)
        {
            base.OnPostExecute(result);
            progressDialog.Hide();
            taskCompleted.OnTaskCompleted(result);
        }
    }
}