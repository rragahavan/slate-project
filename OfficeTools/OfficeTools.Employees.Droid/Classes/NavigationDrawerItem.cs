using System;

namespace OfficeTools.Employees.Droid.Classes
{
    public class NavigationDrawerItem
    {
        public string Title { get; set; }
        public int Icon { get; set; }
        public NavigationDrawerItem()
        {

        }
        public NavigationDrawerItem(String title,int icon)
        {
            Title = title;
            Icon = icon;
        }

    }
}