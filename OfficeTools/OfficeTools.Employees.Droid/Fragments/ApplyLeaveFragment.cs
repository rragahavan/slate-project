using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Employees.Droid.Repositories;
using OfficeTools.Shared.Models;
using System;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class ApplyLeaveFragment : Fragment
    {
        private EditText startDateText;
        private EditText endDateText;
        private EditText commentText;
        private EditText numberOfDaysTexts;
        private DateTime startDate;
        private DateTime endDate;
        private string endDateValue;
        private string startDateValue;
        private string leaveTypeValue = "";
        private float numberOfDays;
        private Spinner leaveTypeSpinner;
        private Spinner firstHalfTypeSpinner;
        private Spinner secondHalfTypeSpinner;
        List<DateTime> bankHolidays = new List<DateTime>();
        List<string> halfDayType = new List<string>();
        List<string> secondHalfDayType = new List<string>();
        private string template;
        private string toDateHalfValue, fromDateHalfValue;
        private List<string> leaveTypeList = new List<string>();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
            Clear();
        }

        private void Clear()
        {
            startDate = DateTime.MinValue;
            endDate = DateTime.MinValue;
            startDateText.Text = "";
            endDateText.Text = "";
            numberOfDaysTexts.Text = "";
            commentText.Text = "";
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.ApplyLeaveLayout, container, false);
            Button applyButton = view.FindViewById<Button>(Resource.Id.applyButton);
            startDateText = view.FindViewById<EditText>(Resource.Id.startDateText);
            endDateText = view.FindViewById<EditText>(Resource.Id.endDateText);
            commentText = view.FindViewById<EditText>(Resource.Id.commentText);
            numberOfDaysTexts = view.FindViewById<EditText>(Resource.Id.numberOfDaysText);
            firstHalfTypeSpinner = view.FindViewById<Spinner>(Resource.Id.startDateHalfDay);
            secondHalfTypeSpinner = view.FindViewById<Spinner>(Resource.Id.endDateHalfDay);

            startDateText.Click += StartDateText_Click;
            endDateText.Click += EndDate_Click;
            applyButton.Click += ApplyButton_Click;
            leaveTypeSpinner = view.FindViewById<Spinner>(Resource.Id.typeOfLeave);
            leaveTypeList.Clear();
            leaveTypeList.Add(GetString(Resource.String.LeaveType));
            halfDayType.Add("First Half");
            halfDayType.Add("Second Half");
            ArrayAdapter<String> leaveHalfDayTypeArrayAdapter = new ArrayAdapter<String>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, halfDayType);
            leaveHalfDayTypeArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            firstHalfTypeSpinner.Adapter = leaveHalfDayTypeArrayAdapter;


            secondHalfDayType.Add("Second Half");
            secondHalfDayType.Add("First Half");
            ArrayAdapter<String> leaveSecondDayTypeArrayAdapter = new ArrayAdapter<String>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, secondHalfDayType);
            leaveSecondDayTypeArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            secondHalfTypeSpinner.Adapter = leaveSecondDayTypeArrayAdapter;
            Fetch();


            firstHalfTypeSpinner.ItemSelected += FirstHalfTypeSpinner_ItemSelected;
            secondHalfTypeSpinner.ItemSelected += SecondHalfTypeSpinner_ItemSelected;
            leaveTypeSpinner.ItemSelected += LeaveTypeSpinner_ItemSelected;
            return view;
        }

        private void SecondHalfTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {

            Spinner spinner = (Spinner)sender;
            toDateHalfValue = spinner.GetItemAtPosition(e.Position).ToString();
            if (endDate != DateTime.MinValue && startDate != DateTime.MinValue)
            {
                numberOfDays = CalculateLeaveDays();
                DisplayNoOfDays();
            }
            //else
            //    Display(GetString(Resource.String.select_date));
        }

        private void FirstHalfTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            fromDateHalfValue = spinner.GetItemAtPosition(e.Position).ToString();

            if (endDate != DateTime.MinValue && startDate != DateTime.MinValue)
            {
                numberOfDays = CalculateLeaveDays();
                DisplayNoOfDays();
            }
            //else
            //    Display(GetString(Resource.String.select_date));
        }

        private void LeaveTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            leaveTypeValue = spinner.GetItemAtPosition(e.Position).ToString();
        }

        private void StartDateText_Click(object sender, EventArgs e)
        {

            startDateText.Text = "";
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                startDateText.Text = time.ToShortDateString();
                startDateValue = time.ToString("yyyy/MM/dd");
                startDate = time;
                if (endDate != DateTime.MinValue)
                {
                    numberOfDays = CalculateLeaveDays();
                    DisplayNoOfDays();
                }
            });
            frag.Show(this.Activity.FragmentManager, "fragment");
        }

        private void EndDate_Click(object sender, EventArgs e)
        {
            endDateText.Text = "";
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                endDateText.Text = time.ToShortDateString();
                endDateValue = time.ToString("yyyy/MM/dd");
                endDate = time;
                if (startDate != DateTime.MinValue)
                {
                    numberOfDays = CalculateLeaveDays();
                    DisplayNoOfDays();
                }
            });
            frag.Show(this.Activity.FragmentManager, "fragment");

        }


        private void ApplyButton_Click(object sender, EventArgs e)
        {
            string comments = commentText.Text;
            if (startDate == DateTime.MinValue || endDate == DateTime.MinValue)
            {
                Display(GetString(Resource.String.select_date));
                return;
            }

            if (leaveTypeValue.Equals(GetString(Resource.String.LeaveType)))
            {
                Display(GetString(Resource.String.select_leave_type));
                return;
            }
            if (numberOfDays <= 0)
            {
                Display(GetString(Resource.String.check_to_date));
                return;
            }
            if (comments.Equals(""))
            {
                Display(GetString(Resource.String.comment_blank));
                return;
            }
            // var leaveData = new LeaveRequest { template = template, fromDate = startDateValue, leaveType = leaveTypeValue, numDays = numberOfDays, toDate = endDateValue, UserComments = comments };
            var leaveData = new LeaveRequest { template = template, fromDate = startDateValue, leaveType = leaveTypeValue, toDate = endDateValue, UserComments = comments };
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                var response = new Repositories.EmployeeRepository(this.Activity).AddLeaveDetails(leaveData);
                if (response.Success)
                {
                    Display(GetString(Resource.String.LeaveAdded));
                    Clear();
                }
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
        }

        public float CalculateLeaveDays()
        {
            DateTime firstDay = startDate;
            DateTime lastDay = endDate;
            float businessDays = (float)(lastDay - firstDay).TotalDays + 1;
            var i = firstDay;
            if (businessDays > 0)
            {
                while (i <= lastDay)
                {
                    if ((int)i.DayOfWeek == 6 || i.DayOfWeek == 0 || Validate(i) != -1)
                        businessDays--;
                    i = i.AddDays(1);
                }
                if (businessDays > 0)
                {
                    if (firstDay == lastDay)
                    {
                        if ((fromDateHalfValue == "First Half" && toDateHalfValue == "First Half")
                           || (fromDateHalfValue == "Second Half" && toDateHalfValue == "Second Half"))
                        {
                            businessDays = businessDays - (float)0.5;
                        }

                        if (fromDateHalfValue == "Second Half" && toDateHalfValue == "First Half")
                            businessDays = -1;

                    }
                    else
                    {

                        if (fromDateHalfValue == "Second Half" && (!((int)firstDay.DayOfWeek == 6 || firstDay.DayOfWeek == 0 || Validate(firstDay) != -1)))
                            businessDays = businessDays - (float)0.5;
                        if (toDateHalfValue == "First Half" && (!((int)lastDay.DayOfWeek == 6 || lastDay.DayOfWeek == 0 || Validate(lastDay) != -1)))
                            businessDays = businessDays - (float)0.5;
                    }
                }
            }

            return businessDays > 0 ? businessDays : 0;
        }

        public void DisplayNoOfDays()
        {
            if (numberOfDays != 0)
                numberOfDaysTexts.Text = numberOfDays.ToString();
            else
            {
                Display(GetString(Resource.String.ValdateDate));
                numberOfDaysTexts.Text = "0";
            }
        }

        public int Validate(DateTime holiday)
        {
            foreach (DateTime bankHoliday in bankHolidays)
            {
                if (holiday == bankHoliday.Date)
                    return 0;
            }
            return -1;
        }


        public async void Fetch()
        {
            await FetchData();
            ArrayAdapter<String> leaveTypeArrayAdapter = new ArrayAdapter<String>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, leaveTypeList);
            leaveTypeArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            leaveTypeSpinner.Adapter = leaveTypeArrayAdapter;
        }

        async System.Threading.Tasks.Task FetchData()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                var settingList = new EmployeeRepository(this.Activity).GetSettings();
                if (!settingList.success)
                    return;
                else
                {
                    foreach (var setting in settingList.data)
                    {
                        template = setting.MailTemplate;
                        foreach (var leaveType in setting.LeaveTypes)
                            leaveTypeList.Add(leaveType.Description);
                        foreach (var holidays in setting.Holidays)
                            bankHolidays.Add(Convert.ToDateTime(holidays.Date));
                    }
                }
            });
        }




        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }
    }
}