using System;
using Android.App;
using Android.OS;
using Android.Widget;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class CustomDatePickerFragment : DialogFragment, DatePickerDialog.IOnDateSetListener
    {
        Action<DateTime> dateSelectedHandler = delegate { };

        public static CustomDatePickerFragment NewInstance(Action<DateTime> onDateSelected)
        {
            CustomDatePickerFragment frag = new CustomDatePickerFragment();
            frag.dateSelectedHandler = onDateSelected;
            return frag;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            DateTime currently = DateTime.Now;
            DatePickerDialog dialog = new DatePickerDialog(Activity,
                                                           this,
                                                           currently.Year,
                                                           currently.Month - 1,
                                                           currently.Day);
            var origin = new DateTime(1970, 1, 1);
            dialog.DatePicker.MinDate = 0;
            dialog.DatePicker.MaxDate = (long)(DateTime.Now.Date - origin).TotalMilliseconds;
            return dialog;
        }

        public void OnDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {

            DateTime selectedDate = new DateTime(year, monthOfYear + 1, dayOfMonth);
            dateSelectedHandler(selectedDate);
        }
    }
}