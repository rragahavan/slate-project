using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using OfficeTools.Shared.Models;
using Com.Nostra13.Universalimageloader.Core;
using Android.Support.V4.App;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class SpotlightFragment : Fragment
    {
        public string FirstName { get; set; }
        public string ProfileImage { get; set; }
        public string EmployeeEmail { get; set; }
        


        public SpotlightFragment(SpotlightEmployee spotlightEmployee)
        {
            FirstName = spotlightEmployee.FirstName;
            ProfileImage = spotlightEmployee.ProfileImage;
            EmployeeEmail = spotlightEmployee.Email;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.RecyclerSpotlightItems, container, false);
            TextView firstName = view.FindViewById<TextView>(Resource.Id.spotlightEmployeeNameText);
            ImageView ProfileImageView = view.FindViewById<ImageView>(Resource.Id.spotlightEmployeeImage);
            TextView Email = view.FindViewById<TextView>(Resource.Id.spotlightEmployeeEmailText);
            DisplayImageOptions displayImageOption;
            ImageLoader imageLoader = ImageLoader.Instance;
            displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
            .Build();
            imageLoader.DisplayImage(this.ProfileImage, ProfileImageView,
                displayImageOption, null);
            firstName.Text = this.FirstName;
            if (!string.IsNullOrEmpty(EmployeeEmail))
                Email.Text = EmployeeEmail;
            return view;
        }
    }
}