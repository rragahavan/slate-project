using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Employees.Droid.Repositories;
using OfficeTools.Shared.Models;
using System;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class TimesheetEntryFragment : Fragment
    {
        private EditText taskDateText;
        private string taskNameValue;
        private string projectNameValue;
        private string taskDateValue;
        private EditText timeSpentText;
        private EditText descriptionText;
        private EditText taskNameText;
        private Spinner projectNameSpinner;
        private List<string> projectTypeList = new List<string>();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.TimesheetEntryLayout, container, false);
            taskDateText = view.FindViewById<EditText>(Resource.Id.taskDateText);
            timeSpentText = view.FindViewById<EditText>(Resource.Id.timeSpentText);
            descriptionText = view.FindViewById<EditText>(Resource.Id.descriptionTaskText);
            Button addTimesheetButton = view.FindViewById<Button>(Resource.Id.addTimesheetButton);
            taskNameText = view.FindViewById<EditText>(Resource.Id.taskNameText);
            projectNameSpinner = view.FindViewById<Spinner>(Resource.Id.projectNameSpinner);
            projectTypeList.Add(GetString(Resource.String.ProjectName));
            if (AppUtil.IsNetworkAvailable(this.Activity))
            {
                Android.App.ProgressDialog myProgressBar = new Android.App.ProgressDialog(this.Activity);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                Fetch();
                myProgressBar.Hide();
            }
            taskDateText.Click += TaskDateText_Click;
            projectNameSpinner.ItemSelected += ProjectNameSpinner_ItemSelected;
            Spinner taskNameSpinner = view.FindViewById<Spinner>(Resource.Id.taskNameSpinner);
            var taskAdapter = ArrayAdapter.CreateFromResource(
                this.Activity, Resource.Array.task_names, Resource.Layout.Spinner_item);
            taskAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            taskNameSpinner.Adapter = taskAdapter;
            taskNameSpinner.ItemSelected += TaskNameSpinner_ItemSelected;

            addTimesheetButton.Click += delegate
            {
                var descrptionInformation = descriptionText.Text;
                var timeSpent = timeSpentText.Text;
                taskNameValue = taskNameText.Text;

                if (string.IsNullOrEmpty(projectNameValue))
                {
                    Display(GetString(Resource.String.SelectProjectName));
                    return;
                }

                if (string.IsNullOrEmpty(taskDateValue))
                {
                    Display(GetString(Resource.String.select_date));
                    return;
                }
                if (string.IsNullOrEmpty(timeSpent))
                {
                    Display(GetString(Resource.String.SelectTimeSpent));
                    return;
                }

                if (string.IsNullOrEmpty(taskNameValue))
                {
                    Display(GetString(Resource.String.SelectTaskName));
                    return;
                }
                if (string.IsNullOrEmpty(descrptionInformation))
                {
                    Display(GetString(Resource.String.EnterDescription));
                    return;
                }
                var timesheetData = new Timesheet { taskDate = taskDateValue, projectName = projectNameValue, task = taskNameValue, hours = timeSpent, description = descrptionInformation };
                if (AppUtil.IsNetworkAvailable(this.Activity))
                {
                    var addTimesheetResponse = new EmployeeRepository(this.Activity).AddTimesheet(timesheetData);
                    if (addTimesheetResponse.success)
                        Toast.MakeText(this.Activity, Resource.String.AddTimesheet, ToastLength.Long).Show();
                }
                else
                    Toast.MakeText(this.Activity, Resource.String.NoInternetConnection, ToastLength.Long).Show();
            };

            return view;

        }

       


        private void TaskDateText_Click(object sender, EventArgs e)
        {
            taskDateText.Text = "";
            CustomDatePickerFragment frag = CustomDatePickerFragment.NewInstance(delegate (DateTime time)
            {
                taskDateText.Text = time.ToShortDateString();
                taskDateValue = time.ToString("yyyy/MM/dd");

            });

            frag.Show(this.Activity.FragmentManager, "fragment");
        }

        private void TaskNameSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            taskNameValue = spinner.GetItemAtPosition(e.Position).ToString();
        }

        private void ProjectNameSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            string selectedValue=spinner.GetItemAtPosition(e.Position).ToString();
            
            if (!selectedValue.Equals(GetString(Resource.String.ProjectName)))
            projectNameValue =selectedValue;
        }

        public async void Fetch()
        {
            await FetchData();
            ArrayAdapter<String> projectTypeArrayAdapter = new ArrayAdapter<String>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, projectTypeList);
            projectTypeArrayAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            projectNameSpinner.Adapter = projectTypeArrayAdapter;
        }

        async System.Threading.Tasks.Task FetchData()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                var settingList = new EmployeeRepository(this.Activity).GetSettings();
                if (!settingList.success)
                    return;
                else
                {
                    foreach (var setting in settingList.data)                      
                        foreach (var projectType in setting.ProjectTypes)
                            projectTypeList.Add(projectType.name);
                }
            });
        }

        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }

    }
}