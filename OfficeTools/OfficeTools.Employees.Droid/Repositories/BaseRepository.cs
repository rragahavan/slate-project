using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Shared;
using OfficeTools.Shared.Common;
using RestSharp;
using System;

namespace OfficeTools.Employees.Droid.Repositories
{
    public class BaseRepository
    {
        public IRestResponse InvokeApi(string service, string method, object parameter = null)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            //var client = new RestClient(new Uri(ApiService.DEMO_URL));
            ServiceRequest serviceRequest = new ServiceRequest(service, method, parameter);
            var request = new RestRequest("api/invoke", Method.POST);
            string Authorization = "\"" + AppPreferences.GetAccessToken() + "\"";
            request.AddHeader("Authorization", Authorization);
            request.AddJsonBody(serviceRequest);
            return client.Execute(request);
        }

        public IRestResponse Upload(string fileName, byte[] fileContents)
        {
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            //var client = new RestClient(new Uri(ApiService.DEMO_URL));
            var request = new RestRequest("api/oibfileupload", Method.POST);
            string Authorization = "\"" + AppPreferences.GetAccessToken() + "\"";
            request.AddHeader("Authorization", Authorization);
            request.AddFile("file", fileContents, fileName, "application/octet-stream");
            return client.Execute(request);
        }
    }
}