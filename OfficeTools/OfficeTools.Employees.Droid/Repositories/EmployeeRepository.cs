using Android.Content;
using Newtonsoft.Json;
using OfficeTools.Shared;
using OfficeTools.Shared.Common;
using OfficeTools.Shared.Models;
using OfficeTools.Shared.Response;
using RestSharp;
using System;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Repositories
{
    public class EmployeeRepository : BaseRepository
    {
        private readonly Context context;

        public EmployeeRepository(Context context)
        {
            this.context = context;
        }

        public EmployeeListResponse GetEmployeeList(int page)
        {
            EmployeeListResponse empList;
            var parameter = new { page = page, filter = "Active" };
            var response = InvokeApi("hrms", "employeelist", parameter);
            empList = JsonConvert.DeserializeObject<EmployeeListResponse>(response.Content);
            return empList;
        }

        public AttendanceResponse GetAttendanceList()
        {
            AttendanceResponse attendanceList;
            var response = InvokeApi("hrms", "getAttendanceList");
            attendanceList = JsonConvert.DeserializeObject<AttendanceResponse>(response.Content);
            return attendanceList;
        }

        public DashboardResponse GetDashboardDetails()
        {
            DashboardResponse dashboradList;
            var response = InvokeApi("hrms", "getDashboardData");
            dashboradList = JsonConvert.DeserializeObject<DashboardResponse>(response.Content);
            return dashboradList;
        }


        public SaveAttendanceResponse SaveAttendanceDetails(string status, string CheckInDate, object locationDetail)
        {
            SaveAttendanceResponse attendanceList;
            var parameter = new { status = status, checkIn = CheckInDate, locationDetails = locationDetail };
            var response = InvokeApi("hrms", "saveAttendance", parameter);
            attendanceList = JsonConvert.DeserializeObject<SaveAttendanceResponse>(response.Content);
            return attendanceList;
        }

        public EmployeeResponse GetEmployeeDetails(string empCode)
        {
            EmployeeResponse empList;
         
            var parameter = new { empId = empCode };
                //var parameter = new { empcode = empCode };
            var response = InvokeApi("hrms", "getemployee", parameter);
            empList = JsonConvert.DeserializeObject<EmployeeResponse>(response.Content);
            return empList;
        }

        public LoginResponse GetAccessToken(string accessToken)
        {
            LoginResponse loginResponse;
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            //var client = new RestClient(new Uri(ApiService.DEMO_URL));
            var parameter = new { token = "\"" + accessToken + "\"" };
            var request = new RestRequest("api/login", Method.POST);
            request.AddJsonBody(parameter);
            var respone = client.Execute(request);
            loginResponse = JsonConvert.DeserializeObject<LoginResponse>(respone.Content);
            return loginResponse;
        }

        public LeaveResponse GetLeaveDetails()
        {
            LeaveResponse leaveList;
            var response = InvokeApi("lms", "getLeaves");
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public List<Holiday> GetHolidaysDetails()
        {
            var response = InvokeApi("hrms", "settings");
            var holidays = JsonConvert.DeserializeObject<SettingsResponse>(response.Content).data[0].Holidays;
            return holidays;

        }

        public LeaveResponse AddLeaveDetails(LeaveRequest leave)
        {
            LeaveResponse leaveList;
            var leaveObject = new { leave };
            var parameter = leaveObject;
            var response = InvokeApi("lms", "applyLeave", parameter);
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public SettingsResponse GetSettings()
        {
            SettingsResponse settingList;
            var response = InvokeApi("hrms", "settings");
            settingList = JsonConvert.DeserializeObject<SettingsResponse>(response.Content);
            return settingList;
        }

        public LeaveResponse UpadteLeavestatus(object leaveDetail)
        {
            LeaveResponse leaveList;
            var parameter = leaveDetail;
            var response = InvokeApi("lms", "changeStatus", parameter);
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public LeaveResponse UpadteAllLeavestatus(object leaveDetail)
        {
            LeaveResponse leaveList;
            var parameter = leaveDetail;
            var response = InvokeApi("lms", "changeStatus", parameter);
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public SaveTimesheetResponse AddTimesheet(Timesheet timesheet)
        {
            SaveTimesheetResponse timesheetList;
            var timesheetObject = new { timesheet };
            var parameter = timesheetObject;
            var response = InvokeApi("tms", "saveTimesheet", parameter);
            timesheetList = JsonConvert.DeserializeObject<SaveTimesheetResponse>(response.Content);
            return timesheetList;

        }


        public TimesheetEntryResponse GetTimesheetEntry(string startDate, string endDate)
        {
            TimesheetEntryResponse timesheetEntryResponseList;
            var parameter = new { startDate = startDate, endDate = endDate };
            var response = InvokeApi("tms", "getTimesheet", parameter);
            timesheetEntryResponseList = JsonConvert.DeserializeObject<TimesheetEntryResponse>(response.Content);
            return timesheetEntryResponseList;
        }

        public SubmitTimesheetResponse SaveTimesheet(TimesheetRequest timesheetRequest)
        {
            SubmitTimesheetResponse submitTimesheetResponseList;
            var parameter = new { timesheetRequest };
            var response = InvokeApi("tms", "submitTimesheet", parameter);
            submitTimesheetResponseList = JsonConvert.DeserializeObject<SubmitTimesheetResponse>(response.Content);
            return submitTimesheetResponseList;
            
        }


        public ClaimImageResponse FileUpload(string filename, byte[] fileContent)
        {
            ClaimImageResponse claimImageList;
            var response = Upload(filename, fileContent);
            claimImageList = JsonConvert.DeserializeObject<ClaimImageResponse>(response.Content);
            return claimImageList;
        }

        public SaveClaimResponse SaveClaim(Claims claimDetail)
        {
            SaveClaimResponse saveClaimList;
            var claims = claimDetail;
            var parameter = new { claims };
            var response = InvokeApi("finance", "saveClaim", parameter);
            saveClaimList = JsonConvert.DeserializeObject<SaveClaimResponse>(response.Content);
            return saveClaimList;

        }

    }
}