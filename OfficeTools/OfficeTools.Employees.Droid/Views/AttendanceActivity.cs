using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Employees.Droid.Repositories;
using OfficeTools.Shared.Response;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "AttendanceActivity")]
    public class AttendanceActivity : NavigationDrawerActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AttendaceLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            RecyclerView attendanceRecyclerView = FindViewById<RecyclerView>(Resource.Id.attendanceRecyclerView);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Attendance);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            DateTime today = DateTime.Today;
            string todaysDate = Convert.ToDateTime(today).ToString("MMM dd, yyyy");
            TextView todaysDateText = FindViewById<TextView>(Resource.Id.todaysDateText);
            todaysDateText.Text = todaysDate;

            if (AppUtil.IsNetworkAvailable(this))
            {
                var attendanceList = new EmployeeRepository(this).GetAttendanceList();
                if (attendanceList == null)
                    return;
                if (!attendanceList.Success)
                    return;
                else
                {
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    attendanceRecyclerView.SetLayoutManager(mLayoutManager);
                    AttendanceAdapter attendanceAdapter = new AttendanceAdapter(attendanceList.Data, this);
                    attendanceRecyclerView.SetAdapter(attendanceAdapter);
                }
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }

        }

        public class AttendanceViewHolder : RecyclerView.ViewHolder
        {
            public TextView CheckIn { get; private set; }
            public TextView CheckOut { get; private set; }
            public TextView CheckInLocation { get; private set; }
            public TextView CheckOutLocation { get; private set; }
            public TextView HoursText { get; private set; }

            public AttendanceViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                CheckIn = itemView.FindViewById<TextView>(Resource.Id.checkInText);
                HoursText = itemView.FindViewById<TextView>(Resource.Id.hoursText);
                CheckOut = itemView.FindViewById<TextView>(Resource.Id.checkOutText);
                CheckInLocation = itemView.FindViewById<TextView>(Resource.Id.checkInLocationText);
                CheckOutLocation = itemView.FindViewById<TextView>(Resource.Id.checkOutLocationText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }
        }

        public class AttendanceAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;

            public List<Attendance> attendanceList;
            public Context context;

            public AttendanceAdapter(List<Attendance> attendanceList, Context context)
            {
                this.attendanceList = attendanceList;
                this.context = context;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.AttendanceRecylcerItems, parent, false);
                AttendanceViewHolder vh = new AttendanceViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                AttendanceViewHolder vh = holder as AttendanceViewHolder;
                var attendance = attendanceList[position];
                if (!string.IsNullOrWhiteSpace(attendance.CheckIn))
                {
                    vh.CheckIn.Text = DateTime.Parse(attendance.CheckIn).ToString("HH:mm");
                    if (!string.IsNullOrWhiteSpace(attendance.CheckInLocationDetails.Location))
                        vh.CheckInLocation.Text = attendance.CheckInLocationDetails.Location;
                }
                if (!string.IsNullOrWhiteSpace(attendance.CheckOut))
                {
                    vh.CheckOut.Text = DateTime.Parse(attendance.CheckOut).ToString("HH:mm");
                    if (!string.IsNullOrEmpty(attendance.CheckInLocationDetails.Location))
                        vh.CheckOutLocation.Text = attendance.CheckInLocationDetails.Location;
                    double hours = double.Parse(attendance?.Hours, CultureInfo.InvariantCulture);
                    var hoursDouble = Math.Round(hours, 2);
                    vh.HoursText.Text = hoursDouble.ToString();
                }
            }

            public override int ItemCount
            {
                get { return attendanceList.Count; }
            }

            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        public override void OnBackPressed()
        {
            Intent i = new Intent(this, typeof(QuickPageActivity));
            StartActivity(i);
            Finish();
        }

    }
}