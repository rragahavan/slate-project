using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using OfficeTools.Employees.Droid.Adapters;
using OfficeTools.Employees.Droid.Classes;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "HolidayActivity")]
    public class HolidayActivity : NavigationDrawerActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HolidaysLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Holidays);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            RecyclerView holidayRecyclerView = FindViewById<RecyclerView>(Resource.Id.holidayRecyclerView);

            if (AppUtil.IsNetworkAvailable(this))
            {
                var holidaysList = new Repositories.EmployeeRepository(this).GetHolidaysDetails();
                RecyclerView.LayoutManager holidayLayoutManager = new LinearLayoutManager(this);
                holidayRecyclerView.SetLayoutManager(holidayLayoutManager);
                HolidayAdapter holidayAdapter = new HolidayAdapter(holidaysList, this);
                holidayRecyclerView.SetAdapter(holidayAdapter);
            }
            else
                Toast.MakeText(this, Resource.String.NoInternetConnection, ToastLength.Long).Show();
        }

        public override void OnBackPressed()
        {
            Intent i = new Intent(this, typeof(QuickPageActivity));
            StartActivity(i);
            Finish();
        }

    }
}
