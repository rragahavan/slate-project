using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Uri = Android.Net.Uri;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "HrmsActivity")]
    public class HrmsActivity : NavigationDrawerActivity
    {
        private List<Employee> employeeDetails = new List<Employee>();
        private int pageNumber = 1;
        private int totalPages = 1;
        private EmployeeAdapter mAdapter;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HrmsLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Employees);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
                RecyclerView mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
                if (mRecyclerView != null)
                {
                    mRecyclerView.HasFixedSize = true;
                    var layoutManager = new LinearLayoutManager(this);
                    var onScrollListener = new EmployeeRecyclerViewOnScrollListener(layoutManager);
                    onScrollListener.LoadMoreEvent += async (object sender, EventArgs e) =>
                    {
                        if (AppUtil.IsNetworkAvailable(this))
                        {
                            if (pageNumber < totalPages)
                            {
                                pageNumber = pageNumber + 1;
                                ProgressDialog myProgressBar1 = new ProgressDialog(this);
                                myProgressBar1.SetMessage("Contacting server. Please wait...");
                                myProgressBar1.Window.SetGravity(GravityFlags.Bottom);
                                myProgressBar1.Show();
                                await FetchData();
                                myProgressBar1.Hide();
                                mAdapter = new EmployeeAdapter(employeeDetails, this);
                                mRecyclerView.SetAdapter(mAdapter);
                            }
                        }
                        else
                            Display(GetString(Resource.String.NoInternetConnection));
                    };


                    onScrollListener.LoadPreviousEvent += async (object sender, EventArgs e) =>
                    {
                        if (AppUtil.IsNetworkAvailable(this))
                        {
                            if ((pageNumber <= totalPages) && (pageNumber != 1))
                            {
                                pageNumber = pageNumber - 1;
                                ProgressDialog myProgressBar1 = new ProgressDialog(this);
                                myProgressBar1.SetMessage("Contacting server. Please wait...");
                                myProgressBar1.Show();
                                myProgressBar1.Window.SetGravity(GravityFlags.Bottom);
                                await FetchData();
                                myProgressBar1.Hide();
                                mAdapter = new EmployeeAdapter(employeeDetails, this);
                                mRecyclerView.SetAdapter(mAdapter);
                            }
                        }
                        else
                            Display(GetString(Resource.String.NoInternetConnection));
                    };
                    mRecyclerView.AddOnScrollListener(onScrollListener);
                    mRecyclerView.SetLayoutManager(layoutManager);
                    mAdapter = new EmployeeAdapter(employeeDetails, this);
                    mAdapter.ItemClick += OnItemClick;
                    mRecyclerView.SetAdapter(mAdapter);
                }
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }
        }


        void OnItemClick(object sender, int position)
        {

            Intent i = new Intent(this, typeof(SingleUserDetailsActivity));
            i.PutExtra("position", position);
            i.PutExtra("empId", employeeDetails[position].Id);
            i.PutExtra("empcode", employeeDetails[position].EmpCode);
            StartActivity(i);
        }

        public class EmployeeViewHolder : RecyclerView.ViewHolder
        {
            public ImageView ProfileImage { get; private set; }

            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }

            public TextView EmailAddress { get; private set; }

            public TextView MobileNumber { get; private set; }

            public EmployeeViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                ProfileImage = itemView.FindViewById<ImageView>(Resource.Id.imageView);
                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                EmailAddress = itemView.FindViewById<TextView>(Resource.Id.emailAddressText);
                MobileNumber = itemView.FindViewById<TextView>(Resource.Id.mobileText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }

        public class EmployeeAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<Employee> mEmployee;
            public Context context;

            public EmployeeAdapter(List<Employee> employee, Context context)
            {
                mEmployee = employee;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerEmployeeItems, parent, false);
                EmployeeViewHolder vh = new EmployeeViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                EmployeeViewHolder vh = holder as EmployeeViewHolder;
                DisplayImageOptions displayImageOption;
                var emp = mEmployee[position];
                ImageLoader imageLoader = ImageLoader.Instance;
                displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
                .Build();
                imageLoader.DisplayImage(emp.ProfileImage, vh.ProfileImage,
                    displayImageOption, null);
                vh.FirstName.Text = emp.FirstName + " " + emp.LastName;
                vh.Designation.Text = emp.JobTitle;
                vh.EmailAddress.Text = emp.Email;
                vh.MobileNumber.Text = emp.Mobile;

                vh.EmailAddress.Click += delegate
                {
                    var email = new Intent(Intent.ActionSend);
                    email.PutExtra(Intent.ExtraEmail, new string[] { vh.EmailAddress.Text.ToString() });
                    email.SetType("message/rfc822");
                    context.StartActivity(email);

                };

                vh.MobileNumber.Click += delegate
                {
                    Intent callIntent = new Intent(Intent.ActionDial);
                    callIntent.SetData(Uri.Parse("tel:" + mEmployee[position].Mobile));
                    context.StartActivity(callIntent);

                };


            }

            public override int ItemCount
            {
                get { return mEmployee.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }


        async Task FetchData()
        {
            await Task.Run(() =>
            {
                var response = new Repositories.EmployeeRepository(this).GetEmployeeList(pageNumber);
                if (response.Success)
                {
                    employeeDetails = response.Data.Employees;
                    pageNumber = response.Data.Page;
                    totalPages = response.Data.TotalPages;
                }
            });
        }

        public class EmployeeRecyclerViewOnScrollListener : RecyclerView.OnScrollListener
        {
            public delegate void LoadMoreEventHandler(object sender, EventArgs e);
            public event LoadMoreEventHandler LoadMoreEvent;
            public event LoadMoreEventHandler LoadPreviousEvent;
            private LinearLayoutManager LayoutManager;

            public EmployeeRecyclerViewOnScrollListener(LinearLayoutManager layoutManager)
            {
                LayoutManager = layoutManager;
            }

            public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                base.OnScrolled(recyclerView, dx, dy);
                var visibleItemCount = recyclerView.ChildCount;
                var totalItemCount = recyclerView.GetAdapter().ItemCount;
                var pastVisiblesItems = LayoutManager.FindFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                {
                    LoadMoreEvent(this, null);
                }
                if (pastVisiblesItems == 0 && dy < 0)
                {
                    LoadPreviousEvent(this, null);
                }
            }
        }

        public override void OnBackPressed()
        {
            Intent i = new Intent(this, typeof(QuickPageActivity));
            StartActivity(i);
            Finish();
        }

        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this, this, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }

    }
}
