using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Adapters;
using OfficeTools.Employees.Droid.Classes;
using System.Collections.Generic;
using System.Threading.Tasks;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "LeaveManagementActivity", WindowSoftInputMode = SoftInput.AdjustPan)]
    public class LeaveManagementActivity : NavigationDrawerActivity
    {

        private string[] tabTitles = { "My Leave", "Apply Leave", "My Summary", "Leave Request" };
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LeaveTabLayoutMain);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Leaves);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            if (AppUtil.IsNetworkAvailable(this))
            {
                ProgressDialog myProgressBar = new ProgressDialog(this);
                myProgressBar.SetMessage("Contacting server. Please wait...");
                myProgressBar.Show();
                await FetchData();
                myProgressBar.Hide();
            }
            FragmentPagerAdapter myPagerAdapter = new LeavePagerAdapter(SupportFragmentManager, tabTitles, this);
            viewPager.Adapter = myPagerAdapter;
            myPagerAdapter.NotifyDataSetChanged();
            viewPager.SetPageTransformer(true, new ScaleTransformer());
            TabLayout tabLayout = FindViewById<TabLayout>(Resource.Id.sliding_tabs);
            tabLayout.SetupWithViewPager(viewPager);
            tabLayout.SetTabTextColors(Android.Graphics.Color.White, Android.Graphics.Color.White);
            tabLayout.TabSelected += (object sender, TabLayout.TabSelectedEventArgs e) =>
            {
                var tab = e.Tab;
                switch (tab.Position)
                {
                    case 0:
                        viewPager.SetCurrentItem(0, true);
                        toolbarTitle.Text = GetString(Resource.String.MyLeave);

                        break;
                    case 1:
                        viewPager.SetCurrentItem(1, true);
                        toolbarTitle.Text = GetString(Resource.String.ApplyLeave);

                        break;
                    case 2:
                        viewPager.SetCurrentItem(2, true);
                        toolbarTitle.Text = GetString(Resource.String.LeaveSummary);

                        break;

                    case 3:
                        viewPager.SetCurrentItem(3, true);
                        toolbarTitle.Text = GetString(Resource.String.LeaveRequest);
                        break;
                    default:
                        toolbarTitle.Text = GetString(Resource.String.MyLeave);
                        break;
                }


            };


        }
        async Task FetchData()
        {
            await Task.Run(() =>
            {
                var leaveDetails = new Repositories.EmployeeRepository(this).GetLeaveDetails().Data.ManagerleaveRequests;
                if (leaveDetails.Count == 0)
                {
                    var list = new List<string>(tabTitles);
                    list.Remove("Leave Request");
                    tabTitles = list.ToArray();
                }
            });
        }

        public override void OnBackPressed()
        {
            Intent i = new Intent(this, typeof(QuickPageActivity));
            StartActivity(i);
            Finish();
        }

    }
}