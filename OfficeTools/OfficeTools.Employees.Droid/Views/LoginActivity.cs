using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using OfficeTools.Employees.Droid.Classes;
using System;
using System.Linq;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "LoginActivity")]
    public class LoginActivity : AppCompatActivity, IOnTaskCompleted
    {
        private AuthenticationResult authResult = null;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LoginLayout);
            AppPreferences appPreference = new AppPreferences(this);
            if (!AppPreferences.GetAccessToken().Equals(""))
            {
                Intent i = new Intent(this, typeof(QuickPageActivity));
                StartActivity(i);
                Finish();
            }

            Button LogInButton = FindViewById<Button>(Resource.Id.logInButton);
            LogInButton.Click += LogInButton_Click;
        }

        private async void LogInButton_Click(object sender, EventArgs e)
        {
            if (AppUtil.IsNetworkAvailable(this))
            {
                var authContext = new AuthenticationContext(AppConstant.AUTHORITY);
                if (AppPreferences.GetAccessToken().Equals(""))
                    authContext.TokenCache.Clear();
                if (authContext.TokenCache.ReadItems().Count() > 0)
                    authContext = new AuthenticationContext(authContext.TokenCache.ReadItems().First().Authority);
                try
                {
                    authResult = await authContext.AcquireTokenAsync(AppConstant.GRAPHRESOURCEURI, AppConstant.CLIENTID, new Uri(AppConstant.RETURNURI), new PlatformParameters(this));
                    new LoginTask(this, authResult.IdToken, this).Execute();
                }
                catch (Exception ex)
                {
                    CustomToast customMessage = new CustomToast(this, this, ex.Message, true);
                    customMessage.SetGravity(GravityFlags.Top, 0, 0);
                    customMessage.Show();

                }
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }

        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            AuthenticationAgentContinuationHelper.SetAuthenticationAgentContinuationEventArgs(requestCode, resultCode, data);
        }

        public void OnTaskCompleted(Java.Lang.Object result)
        {
            if (!AppPreferences.GetAccessToken().Equals(""))
            {
                Intent i = new Intent(this, typeof(QuickPageActivity));
                StartActivity(i);
                Finish();
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.EmployeeNotPresent), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }
        }
    }
}