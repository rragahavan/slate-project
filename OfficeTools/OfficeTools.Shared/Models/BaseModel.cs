﻿using System.Collections.Generic;

namespace OfficeTools.Shared.Models
{
    public class BaseModel
    {
        public List<string> Errors;
        public BaseModel()
        {
            Errors = new List<string>();
        }

        public void SetErrors(string Error)
        {
            Errors.Add(Error);
        }

        public List<string> GetErrors()
        {
            return Errors;
        }

        public bool GetHasErrors()
        {
            return (this.Errors != null && this.Errors.Count > 0);
        }
    }
}
