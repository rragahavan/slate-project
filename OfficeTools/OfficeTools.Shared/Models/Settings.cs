﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OfficeTools.Shared.Models
{
    public class Role
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class JobTitle
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Grade
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class ShiftType
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class EmpType
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Country
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Holiday
    {
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Currency
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Address
    {
        [JsonProperty("line")]
        public string Line { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("zip")]
        public string Zip { get; set; }
    }

    public class CompanyInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("address")]
        public Address Address { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("fax")]
        public string Fax { get; set; }
        [JsonProperty("website")]
        public string Website { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
    }

    public class ExpenseCategory
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class GeneralSettings
    {
        [JsonProperty("logoPath")]
        public string LogoPath { get; set; }
        [JsonProperty("companyWebsiteUrl")]
        public string CompanyWebsiteUrl { get; set; }
        [JsonProperty("companyTitle")]
        public string CompanyTitle { get; set; }
        [JsonProperty("copyrightName")]
        public string CopyrightName { get; set; }
    }

    public class FinanceManager
    {
        //[JsonProperty("email")]
        public string email { get; set; }
        //[JsonProperty("name")]
        public string name { get; set; }

    }
    public class Project {
        public string name { get; set; }
    }


    public class Settings
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("roles")]
        public List<Role> Roles { get; set; }
        [JsonProperty("jobTitles")]
        public List<JobTitle> JobTitles { get; set; }
        [JsonProperty("grades")]
        public List<Grade> Grades { get; set; }
        [JsonProperty("shiftTypes")]
        public List<ShiftType> ShiftTypes { get; set; }
        [JsonProperty("empTypes")]
        public List<EmpType> EmpTypes { get; set; }
        [JsonProperty("countries")]
        public List<Country> Countries { get; set; }
        [JsonProperty("holidays")]
        public List<Holiday> Holidays { get; set; }
        [JsonProperty("currencies")]
        public List<Currency> Currencies { get; set; }
        [JsonProperty("companyInfo")]
        public List<CompanyInfo> CompanyInfo { get; set; }
        [JsonProperty("expenseCategory")]
        public List<ExpenseCategory> ExpenseCategory { get; set; }
        [JsonProperty("taxTypes")]
        public List<object> TaxTypes { get; set; }
        [JsonProperty("paymentTypes")]
        public List<object> PaymentTypes { get; set; }
        [JsonProperty("projectTypes")]
        public List<Project> ProjectTypes { get; set; }
        [JsonProperty("invoiceTemplate")]
        public string InvoiceTemplate { get; set; }
        [JsonProperty("mailTemplate")]
        public string MailTemplate { get; set; }
        [JsonProperty("leaveTypes")]
        public List<LeaveType> LeaveTypes { get; set; }
        [JsonProperty("claimTemplate")]
        public string ClaimTemplate { get; set; }

        [JsonProperty("generalSettings")]
        public GeneralSettings GeneralSettings { get; set; }
        [JsonProperty("financeManager")]
        public FinanceManager FinanceManager { get; set; }
        [JsonProperty("taskTypes")]
        public List<object> TaskTypes { get; set; }
    }
}
