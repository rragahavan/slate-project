﻿namespace OfficeTools.Shared.Response
{
    public class CalimImage
    {
        public string _id { get; set; }
        public string name { get; set; }
        public string path { get; set; }
    }

    public class ClaimImageResponse
    {
        public bool success { get; set; }
        public CalimImage data { get; set; }
    }

}
