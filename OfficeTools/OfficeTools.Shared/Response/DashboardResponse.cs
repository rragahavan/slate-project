﻿using Newtonsoft.Json;
using OfficeTools.Shared.Models;

namespace OfficeTools.Shared.Response
{
    public class DashboardResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public Dashboard Data { get; set; }
    }
}
