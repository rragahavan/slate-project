﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OfficeTools.Shared
{
    public class EmployeeListData
    {
        [JsonProperty("employees")]
        public List<Employee> Employees { get; set; }
        [JsonProperty("pageSize")]
        public int PageSize { get; set; }
        [JsonProperty("page")]
        public int Page { get; set; }
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("totalPages")]
        public int TotalPages { get; set; }
    }
    public class EmployeeListResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public EmployeeListData Data { get; set; }
    }
}
