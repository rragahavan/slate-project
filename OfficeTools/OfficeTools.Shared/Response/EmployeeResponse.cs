﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OfficeTools.Shared
{
    public class EmployeeResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public Employee Data { get; set; }
    }
}
