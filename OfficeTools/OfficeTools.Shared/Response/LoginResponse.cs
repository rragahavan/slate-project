﻿using Newtonsoft.Json;
using OfficeTools.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OfficeTools.Shared.Response
{
    public class LoginResponse
    {
           [JsonProperty("success")] 
           public bool Success { get; set; }
           [JsonProperty("data")]
           public Login Data { get; set; }
       
    }
}
