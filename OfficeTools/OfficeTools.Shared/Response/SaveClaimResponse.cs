﻿using Newtonsoft.Json;

namespace OfficeTools.Shared.Response
{
     public class SaveClaimResponse
    {
        [JsonProperty("success")]
        public bool success { get; set; }
        [JsonProperty("data")]
        public Data data { get; set; }
    }
}
