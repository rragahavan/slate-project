﻿using OfficeTools.Shared.Models;
using System.Collections.Generic;

namespace OfficeTools.Shared.Response
{
    public class SettingsResponse
    {
        public bool success { get; set; }
        public List<Settings> data { get; set; }
    }
}
