﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OfficeTools.Shared.Response
{

    public class Upserted
    {
        public int index { get; set; }
        public string _id { get; set; }
    }

    public class SubmitTimesheetData
    {
        public int ok { get; set; }
        public int nModified { get; set; }
        public int n { get; set; }
        public List<Upserted> upserted { get; set; }
    }

    public class SubmitTimesheetResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public SubmitTimesheetData Data { get; set; }
    }
}
