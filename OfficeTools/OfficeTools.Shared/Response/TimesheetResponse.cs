﻿using OfficeTools.Shared.Models;

namespace OfficeTools.Shared.Response
{

    public class TimesheetResponse
    {
        public bool success { get; set; }
        public TimesheetRequest data { get; set; }
    }
}
